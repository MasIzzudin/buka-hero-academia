import 'package:flutter/material.dart';

class Palette {
  static Color primary = Color(0xFFFFB015);
  static Color black5 = Color(0xFF778CA3);
  static Color black10 = Color(0xFFA5B1C2);
  static Color black = Color(0xFF000000);
  static Color black15 = Color(0xFFEDEEF1);
  static Color white = Color(0xFFFFFFFF);
}